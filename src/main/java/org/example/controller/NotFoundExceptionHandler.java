package org.example.controller;

import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.*;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import java.util.List;

@Provider
public class NotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {

    @Context
    private HttpHeaders headers;

    @Context
    private Request request;

    @Override
    public Response toResponse(NotFoundException ex) {
        return Response.status(404).entity("").type( getAcceptType()).build();
    }

    private String getAcceptType(){
        List<MediaType> accepts = headers.getAcceptableMediaTypes();
        if (accepts != null && accepts.size() > 0) {
            return accepts.get(0).toString();
        } else {
            return MediaType.APPLICATION_JSON;
        }
    }
}