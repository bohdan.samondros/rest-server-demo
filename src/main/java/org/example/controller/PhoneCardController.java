package org.example.controller;

import jakarta.inject.Singleton;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import org.example.domain.PhoneCard;
import org.example.service.InMemoryPhoneCardDao;
import org.example.service.PhoneCardDao;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@Path("/phone-cards")
@Singleton
public class PhoneCardController {

    @Context
    private UriInfo uriInfo;

    @Context
    private HttpHeaders httpHeaders;

    private final PhoneCardDao phoneCardDao = new InMemoryPhoneCardDao();

    @POST
    public Response create(PhoneCard phoneCard) {
        phoneCardDao.add(phoneCard);

        return Response.created(URI.create(uriInfo.getAbsolutePath().toString() + "/" + phoneCard.getId())).build();
    }

    @GET
    public List<PhoneCard> getAll() {
        return phoneCardDao.getAll();
    }

    @GET
    @Path("/query")
    public List<PhoneCard> findByName(@QueryParam("name") String name) {
        return phoneCardDao.findByName(name);
    }

    @GET
    @Path("/{id}") // /phone-cards/1
    public Response findById(@PathParam("id") long id) {
        Optional<PhoneCard> cardOptional = phoneCardDao.findById(id);
        if (cardOptional.isPresent()) {
            return Response.ok(cardOptional.get()).build();
        } else {
            throw new NotFoundException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") long id) {
        phoneCardDao.remove(id);

        return Response.ok().build();
    }
}
