package org.example.converter;

import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.ParamConverterProvider;
import jakarta.ws.rs.ext.Provider;
import org.example.domain.PhoneNumber;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
public class PhoneNumberConverter implements ParamConverterProvider {
    @Override
    public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
        if(rawType.equals(PhoneNumber.class)) {
            return new ParamConverter<T>() {
                @Override
                public T fromString(String value) {
                    return rawType.cast(new PhoneNumber(value));
                }

                @Override
                public String toString(T value) {
                    return value.toString();
                }
            };
        }

        return null;
    }
}
