package org.example.service;

import org.example.domain.PhoneCard;

import java.util.List;
import java.util.Optional;

public interface PhoneCardDao {
    List<PhoneCard> getAll();
    void add(PhoneCard phoneCard);
    void remove(long id);
    Optional<PhoneCard> findById(long id);
    List<PhoneCard> findByName(String name);
}
