package org.example.service;

import org.example.domain.PhoneCard;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class InMemoryPhoneCardDao implements PhoneCardDao {
    private AtomicLong idCounter = new AtomicLong();
    private Map<Long, PhoneCard> storage =  Collections.synchronizedSortedMap(new TreeMap<>());

    @Override
    public List<PhoneCard> getAll() {
        return List.copyOf(storage.values());
    }

    @Override
    public void add(PhoneCard phoneCard) {
        long id = idCounter.incrementAndGet();
        phoneCard.setId(id);
        storage.put(id, phoneCard);
    }

    @Override
    public void remove(long id) {
        storage.remove(id);
    }

    @Override
    public Optional<PhoneCard> findById(long id) {
        return storage.containsKey(id) ? Optional.of(storage.get(id)) : Optional.empty();
    }

    @Override
    public List<PhoneCard> findByName(String name) {
        return storage.values().stream()
                .filter(val -> val.getName().contains(name))
                .collect(Collectors.toList());
    }
}
