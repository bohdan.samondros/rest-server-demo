package org.example.domain;

import java.util.Objects;

public class PhoneCard2 {

    private long id;

    private PhoneNumber phone;

    private String name;

    public PhoneCard2() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PhoneNumber getPhone() {
        return phone;
    }

    public void setPhone(PhoneNumber phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneCard2 phoneCard = (PhoneCard2) o;
        return id == phoneCard.id &&
                Objects.equals(phone, phoneCard.phone) &&
                Objects.equals(name, phoneCard.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone, name);
    }

    @Override
    public String toString() {
        return "PhoneCard{" +
                "id=" + id +
                ", number='" + phone + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
